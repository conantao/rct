'user strict';
import React,{
  AppRegistry,
  Component,
  ToolbarAndroid,
  StyleSheet,
  View,
  Text,
} from "react-native";


class rct extends Component{
   render(){
    return (
        <ToolbarAndroid
         style={styles.toolbar}
         logo={require("./assets/img/nav/n1.png")}
         title="首页"
          actions={[{title: 'Settings', icon: require('./assets/img/nav/n2.png'), show: 'always'}]}
        />
      );
   }
}

const styles = StyleSheet.create({
toolbar: {
    backgroundColor: '#e9eaed',
    height: 56,
  },
});

AppRegistry.registerComponent("rct",()=>rct);