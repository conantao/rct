'use strict';

import React, {
  AppRegistry,
  Component,
  StyleSheet,
  Image,
  Text,
  View,
  ListView,
  TouchableHighlight
} from 'react-native';


var rct  = React.createClass({
  getInitialState: function() {
    var ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
    return {
      dataSource: ds.cloneWithRows(this._genRows({})),
    };
  },
  _pressData: ({}: {[key: number]: boolean}),
  componentWillMount: function() {
    this._pressData = {};
  },
  render: function() {
    return (
      <ListView
        initialListSize={12}
        contentContainerStyle={styles.list}
        dataSource={this.state.dataSource}
        renderRow={this._renderRow}
      />
    );
  },
  _renderRow: function(rowData: string, sectionID: number, rowID: number) {
    var imgSource = thumb[rowID];
    return (
      <TouchableHighlight underlayColor="red">
        <View>
          <View style={styles.row}>
            <Image style={styles.thumb} source={imgSource} />
            <Text style={styles.text}>
              {rowData}
            </Text>
          </View>
        </View>
      </TouchableHighlight>
    );
  },
  _genRows: function(pressData: {[key: number]: boolean}): Array<string> {
    var dataBlob = [];
    for (var ii = 0; ii < thumb.length; ii++) {
      dataBlob.push('单元格 ' + ii);
    }
    return dataBlob;
  }
});

const styles=StyleSheet.create({

})

const thumb =[
  require("./assets/hands/h1.png"),
  require("./assets/hands/h2.png"),
  require("./assets/hands/h3.png"),
  require("./assets/hands/h4.png"),
  require("./assets/hands/h5.png"),
  require("./assets/hands/h6.png"),
  require("./assets/hands/h7.png"),
  require("./assets/hands/h8.png"),
  require("./assets/hands/h9.png"),
  require("./assets/hands/h10.png"),
  require("./assets/hands/h11.png"),
  require("./assets/hands/h12.png")
];


AppRegistry.registerComponent("rct",()=>rct);