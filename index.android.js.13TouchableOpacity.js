'use strict';

import React, {
  AppRegistry,
  Component,
  StyleSheet,
  Text,
  View,
  TouchableHighlight,
  TouchableOpacity,
} from 'react-native';


class rct extends Component {
  render(){
    return ( 
      <View>
        <TouchableHighlight 
            underlayColor="rgb(210, 230, 255)"
            activeOpacity={0.5}  
            style={{ borderRadius: 8,padding: 6,marginTop:5}}>
               <Text style={{fontSize:16}}>点击我</Text>
        </TouchableHighlight>
        <TouchableOpacity style={{marginTop:20}}>
             <Text style={{fontSize:16}}>点击改变透明度</Text>
        </TouchableOpacity>
      </View>
    )
  }
}

const styles=StyleSheet.create({

})


AppRegistry.registerComponent("rct",()=>rct);