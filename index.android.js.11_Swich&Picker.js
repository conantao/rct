'user strict';
import React,{
  AppRegistry,
  Component,
  Picker,
  Switch,
  StyleSheet,
  View,
  Text,
} from "react-native";


var rct =  React.createClass({
  getInitialState() {
    return {
      trueSwitchIsOn: true,
      falseSwitchIsOn: false,
      language:"请选择"
    };
  },
   render(){
    return (
        <View>
          <Text>Switch链接</Text>
          <Switch onValueChange={(value)=>this.setState({falseSwitchIsOn:value})}
            style={{marginBottom:10,marginTop:10}}
            value={this.state.falseSwitchIsOn}
            />
          <Picker
            selectedValue={this.state.language}
            onValueChange={(lang) => this.setState({language: lang})}>
            <Picker.Item label="Java" value="java" />
            <Picker.Item label="JavaScript" value="js" />
          </Picker>
        </View>
      );
   }
});

const styles = StyleSheet.create({
toolbar: {
    backgroundColor: '#e9eaed',
    height: 56,
  },
});

AppRegistry.registerComponent("rct",()=>rct);