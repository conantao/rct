'user strict';

import React,{
	AppRegistry,
	Component,
	StyleSheet,
	ProgressBarAndroid,
	Text,
	View
} from "react-native";
class rct extends Component {
   render() {
     return (<View>
     	    <Text>ProgressBarAndroid:红圈</Text>
     	    <ProgressBarAndroid color="red" styleAttr='Inverse'/>
            <Text>ProgressBarAndroid:绿线不动</Text>
     	    <ProgressBarAndroid  color="green" styleAttr='Horizontal' progress={0.2} 
            indeterminate={false} style={{marginTop:10}}/>
            <Text>ProgressBarAndroid:绿线动</Text>
            <ProgressBarAndroid  color="green" styleAttr='Horizontal'
            indeterminate={true} style={{marginTop:10}}/>
           <Text>ProgressBarAndroid:小黑圈</Text>
           <ProgressBarAndroid  color="black" styleAttr='SmallInverse'
            style={{marginTop:10}}/>
           <Text>ProgressBarAndroid:大圈</Text>
            <ProgressBarAndroid  styleAttr='LargeInverse'
            style={{marginTop:10}}/>

          </View>
     	);
   	}
}

AppRegistry.registerComponent("rct",()=> rct);